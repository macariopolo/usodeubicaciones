package edu.uclm.esi.usodeubicaciones;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LanzadoraUsoDeUbicaciones {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(LanzadoraUsoDeUbicaciones.class, args);
	}

}
